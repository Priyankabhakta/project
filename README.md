# Protractor E2E Testing
Protractor is an end-to-end test framework for Angular and AngularJS application.Protractor runs test against your application running in real browser,interacting with it as a user world.
# Installation
Node Js
Visual Studio
Type Script
Java Script
Protractor
Jasmine
Protractor-beautiful-reporter
Beautify File
# Concepts Implemented
Used Page Object Model to design testcases for the web application.
Concepts: Assertion,Selection,Alert accept,Submit,Maximizing window,Waiting for non agular,Exception conditions,Matchers,loops,uploading file,OnPrepare,Sending keys,Timeouts,Waits,BeforeEach,AfterEach
# Report
 Used Protractor-beautiful-reporter to create html report for the web application.
